# OSI² Handcart

The [OSI² MRI Scanner](https://gitlab.com/osii/mri-scanners) be used as a mobile system.
This repository contains the technical documentation for a dedicated handcart.

This version has been developed for the [OSI² ONE](https://gitlab.com/osii/mri-scanners/osii-one) system.

## Authors and acknowledgment

- Original design by Wouter Teeuwisse, Leids Universitair Medisch Centrum (LUMC)
- v1.0.0 by Martin Häuer, Physikalisch-Technische Bundesanstalt (PTB)

## License and Liability

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and  [DISCLAIMER](DISCLAIMER.pdf).
