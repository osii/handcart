# Transport Instructions

The entire system can be moved around in a van.
For this, the main magnet has to be separated from the rest of the car, as otherwise it will likely 1) not fit into the van and 2) impossible to lift inside.

This requires (apart from a car and the OSI² ONE system itself):

- a lifting table, max. load ≥ 150kg
- the transport sub-assembly (see BOM)

**The instructions:** (for illustration check the graphic below)

0. Remove all cables that connect between the components at the top (gradient coils, RF coils etc.) with the electronics inside the cart.
1. Place a `frame` onto the lifting table and connect this to `cart` using `lin-con`. Unscrew all screws connecting `top-plate` with `cart` and push `top-plate` onto `frame` (on the lifting table).
2. Place another `frame` into the car and connect `bridge` to it. Connect the other end of `bridge` with the `frame` on the lifting table.
3. Push `top-plate` onto the `frame` in the car.
4. Disconnect `bridge` and start driving :)

![](/res/img/transport.png){width=60%}

